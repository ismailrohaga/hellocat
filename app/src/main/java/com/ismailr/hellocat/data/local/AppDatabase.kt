package com.ismailr.hellocat.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Created by IsmailR on 1/22/21.
 */
@Database(entities = [BreedEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun breedDao(): BreedDao
}
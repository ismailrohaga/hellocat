package com.ismailr.hellocat.data.local

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ismailr.hellocat.data.model.Breed
import com.ismailr.hellocat.data.model.Image
import com.ismailr.hellocat.data.model.Weight

/**
 * Created by IsmailR on 1/22/21.
 */
@Entity(tableName = "breed")
data class BreedEntity(
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,
    @ColumnInfo(name = "adaptability")
    val adaptability: Int? = 0,
    @ColumnInfo(name = "affection_level")
    val affection_level: Int? = 0,
    @ColumnInfo(name = "alt_names")
    val alt_names: String?,
    @ColumnInfo(name = "cfa_url")
    val cfa_url: String?,
    @ColumnInfo(name = "child_friendly")
    val child_friendly: Int? = 0,
    @ColumnInfo(name = "country_code")
    val country_code: String?,
    @ColumnInfo(name = "country_codes")
    val country_codes: String?,
    @ColumnInfo(name = "description")
    val description: String?,
    @ColumnInfo(name = "dog_friendly")
    val dog_friendly: Int? = 0,
    @ColumnInfo(name = "energy_level")
    val energy_level: Int? = 0,
    @ColumnInfo(name = "experimental")
    val experimental: Int? = 0,
    @ColumnInfo(name = "grooming")
    val grooming: Int? = 0,
    @ColumnInfo(name = "hairless")
    val hairless: Int? = 0,
    @ColumnInfo(name = "health_issues")
    val health_issues: Int? = 0,
    @ColumnInfo(name = "hypoallergenic")
    val hypoallergenic: Int? = 0,
    @ColumnInfo(name = "image")
    val image: String?,
    @ColumnInfo(name = "indoor")
    val indoor: Int? = 0,
    @ColumnInfo(name = "intelligence")
    val intelligence: Int? = 0,
    @ColumnInfo(name = "lap")
    val lap: Int? = 0,
    @ColumnInfo(name = "life_span")
    val life_span: String?,
    @ColumnInfo(name = "name")
    val name: String?,
    @ColumnInfo(name = "natural")
    val natural: Int? = 0,
    @ColumnInfo(name = "origin")
    val origin: String?,
    @ColumnInfo(name = "rare")
    val rare: Int? = 0,
    @ColumnInfo(name = "reference_image_id")
    val reference_image_id: String?,
    @ColumnInfo(name = "rex")
    val rex: Int? = 0,
    @ColumnInfo(name = "shedding_level")
    val shedding_level: Int? = 0,
    @ColumnInfo(name = "short_legs")
    val short_legs: Int? = 0,
    @ColumnInfo(name = "social_needs")
    val social_needs: Int? = 0,
    @ColumnInfo(name = "stranger_friendly")
    val stranger_friendly: Int? = 0,
    @ColumnInfo(name = "suppressed_tail")
    val suppressed_tail: Int? = 0,
    @ColumnInfo(name = "temperament")
    val temperament: String?,
    @ColumnInfo(name = "vcahospitals_url")
    val vcahospitals_url: String?,
    @ColumnInfo(name = "vetstreet_url")
    val vetstreet_url: String?,
    @ColumnInfo(name = "vocalisation")
    val vocalisation: Int? = 0,
    @ColumnInfo(name = "weight")
    val weight: String?,
    @ColumnInfo(name = "wikipedia_url")
    val wikipedia_url: String?
    )

fun BreedEntity.toData() = Breed(
    this.adaptability,
    this.affection_level,
    this.alt_names,
    this.cfa_url,
    this.child_friendly,
    this.country_code,
    this.country_codes,
    this.description,
    this.dog_friendly,
    this.energy_level,
    this.experimental,
    this.grooming,
    this.hairless,
    this.health_issues,
    this.hypoallergenic,
    this.id,
    Image(this.image),
    this.indoor,
    this.intelligence,
    this.lap,
    this.life_span,
    this.name,
    this.natural,
    this.origin,
    this.rare,
    this.reference_image_id,
    this.rex,
    this.shedding_level,
    this.short_legs,
    this.social_needs,
    this.stranger_friendly,
    this.suppressed_tail,
    this.temperament,
    this.vcahospitals_url,
    this.vetstreet_url,
    this.vocalisation,
    Weight(this.weight),
    this.wikipedia_url
)

fun breedDummy() = Breed(
        0,
        0,
        null,
        null,
        0,
        null,
        null,
        null,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "cat",
        null,
        0,
        0,
        0,
        null,
        "Cat Breed",
        0,
        null,
        0,
        null,
        0,
        0,
        0,
        0,
        0,
        0,
        null,
        null,
        null,
        0,
        null,
        null
)

fun List<BreedEntity>.toDataList() = this.map { it.toData() }

fun Breed.toDataEntity() = BreedEntity(
    id = this.id,
    adaptability = this.adaptability,
    affection_level = this.affection_level,
    alt_names = this.alt_names,
    cfa_url = this.cfa_url,
    child_friendly = this.child_friendly,
    country_code = this.country_code,
    country_codes = this.country_codes,
    description = this.description,
    dog_friendly = this.dog_friendly,
    energy_level = this.energy_level,
    experimental = this.experimental,
    grooming = this.grooming,
    hairless = this.hairless,
    health_issues = this.health_issues,
    hypoallergenic = this.hypoallergenic,
    image = this.image?.url,
    indoor = this.indoor,
    intelligence = this.intelligence,
    lap = this.lap,
    life_span = this.life_span,
    name = this.name,
    natural = this.natural,
    origin = this.origin,
    rare = this.rare,
    reference_image_id = this.reference_image_id,
    rex = this.rex,
    shedding_level = this.shedding_level,
    short_legs = this.short_legs,
    social_needs = this.social_needs,
    stranger_friendly = this.stranger_friendly,
    suppressed_tail = this.suppressed_tail,
    temperament = this.temperament,
    vcahospitals_url = this.vcahospitals_url,
    vetstreet_url = this.vetstreet_url,
    vocalisation = this.vocalisation,
    weight = this.weight?.metric,
    wikipedia_url = this.wikipedia_url
)

fun List<Breed>.toDataEntityList() = this.map { it.toDataEntity() }
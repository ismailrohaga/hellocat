package com.ismailr.hellocat.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.AnyThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ismailr.hellocat.R
import com.ismailr.hellocat.data.network.Errors
import com.ismailr.hellocat.data.network.Results
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException
import java.lang.Exception

/**
 * Created by IsmailR on 1/22/21.
 */
fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

inline fun Context.toast(value: () -> String) = Toast.makeText(this, value(), Toast.LENGTH_LONG).show()

inline fun log(value: () -> String) = Log.d("${R.string.app_name} DEV_DEBUG", value())

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, observer: (t: T) -> Unit) {
    liveData.observe(this, { it?.let { t -> observer(t) } })
}

@AnyThread
inline fun <reified T> MutableLiveData<T>.postNext(map: (T) -> T) {
    postValue(map(verifyLiveDataNotEmpty()))
}

@AnyThread
inline fun <reified T> LiveData<T>.verifyLiveDataNotEmpty(): T {
    return value
        ?: throw NullPointerException("MutableLiveData<${T::class.java}> not contain value.")
}

inline fun <T> processApiResponse(request: () -> Response<T>): Results<T> {
    return try {
        val response = request()
        val responseCode = response.code()
        if (response.isSuccessful) {
            Results.success(response.body()!!)
        } else {
            return try {
                val responseMessage = JSONObject(response.errorBody()!!.string()).getString("message")
                Results.failure(Errors.NetworkError(responseCode, responseMessage))
            } catch (e: Exception){
                Results.failure(Errors.EmptyResultsError)
            }
        }
    } catch (e: IOException) {
        var message = e.message
        if (message.isNullOrEmpty()) message = "Check your network"
        Results.failure(Errors.NetworkError(-1, message))
    }
}
package com.ismailr.hellocat

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by IsmailR on 1/22/21.
 */
@HiltAndroidApp
class HellocatApplication: Application() {
    init {
        instance = this
    }

    companion object {
        lateinit var instance: HellocatApplication
    }
}
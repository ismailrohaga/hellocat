package com.ismailr.hellocat.base.repository

/**
 * Created by IsmailR on 1/22/21.
 */
open class BaseRepositoryBoth<T : IRemoteDataSource, R : ILocalDataSource>(
    val remoteDataSource: T,
    val localDataSource: R
) : IRepository

@Suppress("unused")
open class BaseRepositoryLocal<T : ILocalDataSource>(
    val remoteDataSource: T
) : IRepository

@Suppress("unused")
open class BaseRepositoryRemote<T : IRemoteDataSource>(
    val remoteDataSource: T
) : IRepository